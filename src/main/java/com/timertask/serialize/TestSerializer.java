package com.timertask.serialize;

import java.io.*;

public class TestSerializer {
    private static final long HEADER = 0x4f2c69a3d70L;
    public static void main(String[] args){
        DataOutputSerializer serializer = new DataOutputSerializer(1024);
        try {
            serializer.writeLong(HEADER);
            serializer.writeLong(System.currentTimeMillis());
            serializer.writeInt(16);
            serializer.writeUTF("0123456789ABCEFG");
            serializer.writeBoolean(true);

            File file = new File(System.getProperty("user.dir") + File.separator + "/test.txt");
            System.out.println(file.getCanonicalFile());
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(serializer.getSharedBuffer());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        //read.
        File file = new File(System.getProperty("user.dir") + File.separator + "/test.txt");
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            byte[] buff = new byte[1024];
            fileInputStream.read(buff);
            fileInputStream.close();
            DataInputDeserializer deserializer = new DataInputDeserializer(buff);
            System.out.println("" + deserializer.readLong());
            System.out.println("" + deserializer.readLong());
            System.out.println("" + deserializer.readInt());
            System.out.println("" + deserializer.readUTF());
            System.out.println("" + deserializer.readBoolean());
        }catch (IOException ee){
            ee.printStackTrace();
        }
    }
}
