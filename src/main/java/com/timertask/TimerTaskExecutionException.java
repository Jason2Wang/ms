package com.timertask;

/**
 * Timer task execution exception.
 * this exception will be thrown at runtime when any task execution encountered error.
 */
public class TimerTaskExecutionException extends Exception {
    public TimerTaskExecutionException(String message) {
        super(message);
    }

    public TimerTaskExecutionException(String message, Throwable cause) {
        super(message, cause);
    }
}
