package com.timertask;

import com.timertask.serialize.DataInputDeserializer;
import com.timertask.serialize.DataOutputSerializer;

import java.io.*;

/**
 * timer Task statistic collected during runtime.
 * this statistics can be serialized and de-serialized during runtime.
 * this class is not thread safe.
 */
public class TaskStatistic implements Serializable{
    //all the parameters are useful for making runtime task execution strategy.
    //TimerTaskManager may use them to separate the tasks and group.

    //job Id key.
    private long jobId;

    //first start time.for accuracy we use milliseconds.
    private long startup;

    //execution interval.this is the initial interval only.it may change due to TimerTaskManager runtime adjustment.
    private long interval;

    // last time execution time.
    private long lastExecutionTime;

    //average execution time.
    private long avgExecutionTime;

    //fastest execution time.
    private long shortestExecutionTime = Long.MAX_VALUE;

    //longest execution time
    private long longestExecutionTime;

    //execution times till now.
    private int times;

    //internal field for marking one single time execution.
    private long singleRunStart; //0 mark stopped or non-started.

    //create an task statistic for tracking the execution runtime data.
    public TaskStatistic(long jobId,long interval){
        this.jobId = jobId;
        this.interval = interval;
        if (getTaskStatisticFile().exists()) {
            deserializationStatistic();
        }
    }

    //track the single time execution.
    public void executionStart(){
        singleRunStart = System.currentTimeMillis();
    }

    //the executionStart has to be executed before this.
    public void executionStop(){
        if(singleRunStart == 0)
            //not started.
            return;
        //calculate
        if(times == 0)
            startup = singleRunStart;
        long now = System.currentTimeMillis();
        long duration = now - singleRunStart;
        lastExecutionTime = duration;
        if(duration<shortestExecutionTime)
            shortestExecutionTime = duration;
        if(duration>longestExecutionTime)
            longestExecutionTime = duration;

        avgExecutionTime = ((avgExecutionTime * times)  + duration ) / (++times);//ten years duration won't exceed the max value.it is quite safe.

        singleRunStart = 0;
        serializeStatistic();
    }

    private File getTaskStatisticFile() {
        File file = new File(System.getProperty("user.dir") + File.separator +  "TaskStatistic" + File.separator + jobId);
        return file;
    }

    private void ensureFileExist(File file) throws IOException {
        File fileParent = file.getParentFile();
        if (!fileParent.exists()) {
            fileParent.mkdirs();
        }
        if (!file.exists())
            file.createNewFile();
    }

    private void serializeStatistic() {
        DataOutputSerializer serializer = new DataOutputSerializer(70);
        try {
            serializer.writeLong(jobId);
            serializer.writeLong(startup);
            serializer.writeLong(interval);
            serializer.writeLong(lastExecutionTime);
            serializer.writeLong(avgExecutionTime);
            serializer.writeLong(shortestExecutionTime);
            serializer.writeLong(longestExecutionTime);
            serializer.writeInt(times);
            serializer.writeLong(singleRunStart);

            File file = getTaskStatisticFile();
            ensureFileExist(file);
            System.out.println(file.getCanonicalFile());
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(serializer.getSharedBuffer());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deserializationStatistic() {
        //read.
        File file = getTaskStatisticFile();
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            byte[] buff = new byte[70];
            fileInputStream.read(buff);
            fileInputStream.close();
            DataInputDeserializer deserializer = new DataInputDeserializer(buff);
            jobId = deserializer.readLong();
            startup = deserializer.readLong();
            interval = deserializer.readLong();
            lastExecutionTime = deserializer.readLong();
            avgExecutionTime = deserializer.readLong();
            shortestExecutionTime = deserializer.readLong();
            longestExecutionTime = deserializer.readLong();
            times = deserializer.readInt();
            singleRunStart = deserializer.readLong();
        }catch (IOException ee){
            ee.printStackTrace();
        }
    }

    //status check
    public boolean isExecutionStarted(){
        return singleRunStart !=0;
    }

    //getter and setter.

    public long getJobId() {
        return jobId;
    }

    public void setJobId(long jobId) {
        this.jobId = jobId;
    }

    public long getStartup() {
        return startup;
    }

    public void setStartup(long startup) {
        this.startup = startup;
    }

    public long getInterval() {
        return interval;
    }

    public void setInterval(long interval) {
        this.interval = interval;
    }

    public long getLastExecutionTime() {
        return lastExecutionTime;
    }

    public void setLastExecutionTime(long lastExecutionTime) {
        this.lastExecutionTime = lastExecutionTime;
    }

    public long getAvgExecutionTime() {
        return avgExecutionTime;
    }

    public void setAvgExecutionTime(long avgExecutionTime) {
        this.avgExecutionTime = avgExecutionTime;
    }

    public long getShortestExecutionTime() {
        return shortestExecutionTime;
    }

    public void setShortestExecutionTime(long shortestExecutionTime) {
        this.shortestExecutionTime = shortestExecutionTime;
    }

    public long getLongestExecutionTime() {
        return longestExecutionTime;
    }

    public void setLongestExecutionTime(long longestExecutionTime) {
        this.longestExecutionTime = longestExecutionTime;
    }

    public int getTimes() {
        return times;
    }

    public void setTimes(int times) {
        this.times = times;
    }

    @Override
    public String toString() {
        return "TaskStatistic{" +
                "jobId=" + jobId +
                ", startup=" + startup +
                ", interval=" + interval +
                ", lastExecutionTime=" + lastExecutionTime +
                ", avgExecutionTime=" + avgExecutionTime +
                ", shortestExecutionTime=" + shortestExecutionTime +
                ", longestExecutionTime=" + longestExecutionTime +
                ", times=" + times +
                ", singleRunStart=" + singleRunStart +
                '}';
    }
}
