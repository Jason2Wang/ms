package com.timertask;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * the basic interface for recurrent task implementation
 * Any task / runnable can implements this class for becoming one manageable task.
 * this class is not thread safe. user code need to consider thread safety if allows parallel execution.
 * TimerTaskManager guarantees to run interval tasks within the second but not the precise milliseconds.
 */
public abstract class TimerTask {

    //task properties.
    //key of task.
    private long jobId ;
    //name.
    private String jobName;
    //runtime statistics.
    private TaskStatistic statistic;//lazily initialization.
    //schedule.
    private TimerSchedule schedule;
    //task priority. 1 - 10. 1 lowest to 10 highest.
    private int priority = 5; //default 5; aligned to Thread.priority

    //preferred task strategy.be aware of ASYNCHRONOUS task strategy won't be considered for changing to other strategy at runtime.
    //SYNCHRONOUS and ANY task strategy might be applying ASYNCHRONOUS strategy at runtime.
    private TaskStrategy preferredStrategy = TaskStrategy.ASYNCHRONOUS;

    public TimerTask(long jobId,TimerSchedule schedule){
        this.jobId = jobId;
        this.schedule = schedule;
        generateJobName();
    }

    public TimerTask(long jobId,String jobName,TimerSchedule schedule){
        this.jobId = jobId;
        this.schedule = schedule;
        this.jobName = jobName;
    }
    /**
     * Job Id.unique job for any given recurrent task.
     * If the job id return from this is not unique by TimerTaskManager, the manager will assign a new job id for this.
     */
    public long getJobId(){
        if(jobId == 0)
            generateJobId();
        return jobId;
    }

    public void setJobId(long jobId){
        this.jobId = jobId;
    }

    /**
     * Job name.customer name.
     */
    public String getJobName(){
        if (jobName == null)
            generateJobName();
        return jobName;
    }

    public void setJobName(String jobName){
        this.jobName = jobName;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        if(priority>10)
            priority = 10;
        if(priority<1)
            priority = 1;
        this.priority = priority;
    }

    public TaskStrategy getPreferredStrategy() {
        return preferredStrategy;
    }

    public void setPreferredStrategy(TaskStrategy preferredStrategy) {
        this.preferredStrategy = preferredStrategy;
    }
    //single time run life cycle

    /**
     * init method for making an task runnable.
     */
    abstract void init();

    /**
     * execution method.recurrent runnable task has no return .
     */
    abstract void run();

    /**
     * release resource.
     */
    abstract void stop();

    //the exception will be thrown if any.
    public final void execute() throws TimerTaskExecutionException {
        //mark start of single time run
        getStatistic().executionStart();
        try {
            init();
            run();
            stop();
        }catch (Exception e){
            throw  new TimerTaskExecutionException("JobId:" + this.jobId + ",JobName:" + this.jobName + " execution error",e);
        }
        finally {
            getStatistic().executionStop();
        }
    }

    // task statistics
    public TaskStatistic getStatistic(){
        if(statistic == null)
            statistic = new TaskStatistic(getJobId(),schedule.getScheduleInterval());
        return statistic;
    }

    //timer schedule.
    public TimerSchedule getTimerSchedule(){
        return schedule;
    }

    public void setTimerSchedule(TimerSchedule schedule){
        this.schedule = schedule;
    }

    //timer task is in single running.
    public boolean isExecutionStarted(){
        return statistic !=null && statistic.isExecutionStarted();
    }

    //equals and hash code.


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TimerTask timerTask = (TimerTask) o;

        return jobId == timerTask.jobId;
    }

    @Override
    public int hashCode() {
        int result = (int) (jobId ^ (jobId >>> 32));
        result = 31 * result + (jobName != null ? jobName.hashCode() : 0);
        result = 31 * result + priority;
        return result;
    }

    @Override
    public String toString() {
        return "TimerTask{" +
                "jobId=" + jobId +
                ", jobName='" + jobName + '\'' +
                ", statistic=" + statistic +
                ", schedule=" + schedule +
                ", priority=" + priority +
                '}';
    }

    //default methods.
    private void generateJobId(){
        setJobId(ThreadLocalRandom.current().nextLong());
    }
     private void generateJobName(){
        setJobName("TimerTask-" + getJobId());
    }

    //static method for wrapping an external runnable as timer task.
     static TimerTask wrap(final Runnable runnable,TimerSchedule schedule){
         long jobId = ThreadLocalRandom.current().nextLong();
        return new TimerTask(jobId,schedule) {
            @Override
            public void init() {

            }
            @Override
            public void run() {
                runnable.run();
            }

            @Override
            public void stop() {

            }
        };
    }

    //helper method and implementation that should be applied for all task
    //according task running statistics , adjust the task cyclic interval
    final void adjustTaskIntervalIfNeeded() {
        //we define 100 execution times threshold for allowing an interval change.
        if (getStatistic().getTimes() == 0 || getStatistic().getTimes() % 100 != 0)
            return;
        //if average execution time is more than schedule interval. set new interval by applying execution time.
        if (getStatistic().getAvgExecutionTime() > getTimerSchedule().getScheduleInterval()) {
            long interval = getTimerSchedule().getScheduleInterval();
            getTimerSchedule().setScheduleInterval(getStatistic().getAvgExecutionTime() + interval);
        }
        //if avg execution time exceeds half of interval. add interval by half.
        else if (getStatistic().getAvgExecutionTime() > getTimerSchedule().getScheduleInterval() >>> 1) {
            long interval = getTimerSchedule().getScheduleInterval();
            getTimerSchedule().setScheduleInterval(interval + interval >>> 1);
        }
    }

    //key step for picking up one timer task for run.
    final boolean isTaskReadyForRun(long clock) {
        if (getStatistic().isExecutionStarted())
            return false;
        long now = System.currentTimeMillis();
        //first time run.check start time.
        if (getStatistic().getTimes() == 0) {
            return getTimerSchedule().getInitialStartTime() == 0 || getTimerSchedule().getInitialStartTime() <= now;
        }
        now = TimeUnit.MILLISECONDS.toSeconds(clock);//internal time.
        //if the schedule is within the second.do it.
        long sec = TimeUnit.MILLISECONDS.toSeconds(getTimerSchedule().getScheduleInterval());
        return now % sec == 0;
    }
}
