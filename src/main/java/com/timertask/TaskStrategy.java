package com.timertask;

/**
 * Timer task execution strategy.It can be dynamically determined or set at initial.
 * it is in ascending safe order
 */
public enum TaskStrategy {
    SYNCHRONOUS,//synchronous running execution strategy.caller thread execute.
    ANY, //both asynchronous or synchronous strategies are acceptable.
    ASYNCHRONOUS,//asynchronous task execution strategy. other thread /thread pool execute.
}
