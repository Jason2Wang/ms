package com.timertask;

import java.util.concurrent.TimeUnit;

/**
 * Timer task is a recurrent task which should have a schedule.The schedule could tell if the task starts at fixed time or at fixed interval.
 */
public class TimerSchedule {

    //fields indicators of timing that the manager properly could run timer task.
    //for accuracy all timestamp are in milliseconds.
    private long initialStartTime;//first start time.
    //recurrent schedule interval.
    private long scheduleInterval;

    /**
     * one timer schedule should have start time and interval.
     * @param startTime start time in timeUnit
     * @param interval interval
     * @param unit timeUnit
     */
    public TimerSchedule(long startTime,long interval, TimeUnit unit){
        if(startTime<=0)
            this.initialStartTime = 0;//0 indicates now.
        if(interval<=0)
            throw new IllegalArgumentException("interval can not be less than 0");
        this.scheduleInterval = unit.toMillis(interval);
        this.initialStartTime = startTime;
    }

    //Getter and setter.


    public long getInitialStartTime() {
        return initialStartTime;
    }

    public void setInitialStartTime(long initialStartTime) {
        this.initialStartTime = initialStartTime;
    }

    public long getScheduleInterval() {
        return scheduleInterval;
    }

    public void setScheduleInterval(long scheduleInterval) {
        this.scheduleInterval = scheduleInterval;
    }

    @Override
    public String toString() {
        return "TimerSchedule{" +
                "initialStartTime=" + initialStartTime +
                ", scheduleInterval=" + scheduleInterval +
                '}';
    }
}
