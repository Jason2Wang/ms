package com.timertask;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class TimerTaskMain {
    public static void main(String[] args){

        TimerTaskManager manager = new TimerTaskManager(10000);
        TimerSchedule tenSecs = new TimerSchedule(0,38, TimeUnit.SECONDS);
        TimerSchedule fifteenSecs = new TimerSchedule(0,19, TimeUnit.SECONDS);
        TimerSchedule twentySecs = new TimerSchedule(0,57, TimeUnit.SECONDS);
        TimerSchedule oneMinute = new TimerSchedule(System.currentTimeMillis()+ 60*1000,76, TimeUnit.SECONDS);
        for(int i=0;i<10;i++){
            TaskA taskA = new TaskA(i,tenSecs);
            taskA.setPreferredStrategy(TaskStrategy.SYNCHRONOUS);
            taskA.setPriority(i);
            try {
                manager.addTask(taskA);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        for(int i=10;i<20;i++){
            TaskA taskA = new TaskA(i,fifteenSecs);
            taskA.setPreferredStrategy(TaskStrategy.ASYNCHRONOUS);
            taskA.setPriority(i%10);
            try {
                manager.addTask(taskA);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        for(int i=20;i<30;i++){
            TaskA taskA = new TaskA(i,twentySecs);
            taskA.setPreferredStrategy(TaskStrategy.ANY);
            taskA.setPriority(i%10);
            try {
                manager.addTask(taskA);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        for(int i=30;i<40;i++){
            TaskA taskA = new TaskA(i,oneMinute);
            taskA.setPriority(i%10);
            try {
                manager.addTask(taskA);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        manager.start();
    }

    static class TaskA extends TimerTask{

        AtomicInteger count = new AtomicInteger(0);

        public TaskA(long jobId, TimerSchedule schedule) {
            super(jobId, schedule);
        }

        public TaskA(long jobId, String jobName, TimerSchedule schedule) {
            super(jobId, jobName, schedule);
        }

        @Override
        void init() {

        }

        @Override
        void run() {
            System.out.println("my job id:" + getJobId() + ",my job name:" + getJobName() + ",count:" + count.incrementAndGet() + ",execution runtime:" + getStatistic());
            Random random = new Random();
            try {
                Thread.sleep(random.nextInt(10000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        @Override
        void stop() {

        }
    }
}
