package com.timertask;

import java.util.*;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The requirement of this timer task manager needs to have features:
 * <p>
 * It can support all the timer tasks adding into this manager.
 * It can decide the minimum interval time for each running for checking all added timer tasks schedule.
 * it can have both synchronous and asynchronous running schedule for the managed tasks pool.
 * it should collect runtime statistics ( each task running execution time, average time, times in a day).
 * Based on the runtime statistics , it can group the tasks pool and separate the running strategy for each group.
 * It is better to provide optimization on running strategy for each pooled tasks.
 * The timer task manager should be common usage and it can be plugged in anywhere (any component or service)
 * Suggestion to make an interface or abstract class define for the common task that your timer task manager support to run.
 * As well the timer task manager should support external tasks that does not implement the task interface / abstract class and wrap that task internally.
 * Please be considerable of saving the running statistics / strategy data into disk in case there is system reboot or running statistic may lose and it takes mounts of data to collect.
 * It can accept external thread pool executors as asynchronous task execution pool. Else it can use default thread pool for asynchronous execution.
 */
public class TimerTaskManager extends Thread {

    //original tasks list.
    private final List<TimerTask> originalTasks = new ArrayList<>(16);
    //internal lock.
    private final Object lock = new Object();
    //thread pool for executing tasks.
    private ThreadPoolExecutor executor;
    //internal clock. start from 0.clock ticking from 0 when startup.it ticks only when task execution.
    private long clock;
    //main manager thread cyclic interval in milliseconds.
    private long cyclicInterval;
    //count of execution times.
    private int count;
    //signal for shutdown.
    private boolean shutdown;

    public TimerTaskManager(ThreadPoolExecutor executor, long cyclicInterval) {
        this.executor = executor;
        if (cyclicInterval <= 0)
            throw new IllegalArgumentException("can not use 0 or negtive value for cyclic interval");
        this.cyclicInterval = cyclicInterval;
    }

    public TimerTaskManager(long cyclicInterval) {
        if (cyclicInterval <= 0)
            throw new IllegalArgumentException("can not use 0 or negtive value for cyclic interval");
        this.cyclicInterval = cyclicInterval;
    }

    public void shutdown() {
        shutdown = true;
        synchronized (lock) {
            lock.notifyAll();
        }
        originalTasks.clear();
    }

    //accept new task.it only accepts cyclic task .
    public synchronized boolean addTask(TimerTask task) throws Exception {
        if (task == null || task.getTimerSchedule() == null || task.getTimerSchedule().getScheduleInterval() == 0)
            throw new Exception("TimerTask needs recurrent schedule");
        if (originalTasks.contains(task))
            throw new Exception("Duplicate task");
        originalTasks.add(originalTasks.size(), task);

        //notify
        synchronized (lock) {
            lock.notify();
        }
        return true;
    }
    //workflow control.it never ends until system shutdown.
    @Override
    public void run() {
        //one single run job.
        long lastExecutionTime = 0;
        applyMostAppropriateCyclicInterval();
        for (; ; ) {
            //check wait time before execution.
            long waitMillis = Math.max(0, cyclicInterval - lastExecutionTime);
            if ((lastExecutionTime == 0 && waitMillis == 0) || waitMillis > 0) {
                synchronized (lock) {
                    try {
                        lock.wait(waitMillis);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (shutdown)
                break;
            ticking();

            //according to last time execution runtime.adjust the cyclic interval.
            applyMostAppropriateCyclicInterval();
            applyTaskIntervalAdjustment();
            //mark a new start.
            long startTime = System.currentTimeMillis();
            List<TaskGroup> taskGroups = buildInitialGroups();
            if (!taskGroups.isEmpty()) {
                TaskGroup synchronousGroup = adjustGroup(taskGroups);
                //asynchronous group execution.
                if (!taskGroups.isEmpty())
                    taskGroups.forEach(this::execute);
                if (synchronousGroup != null)
                    //if select synchronous group from all.
                    execute(synchronousGroup);
            }
            lastExecutionTime = System.currentTimeMillis() - startTime; // mark the execution time for wait.
        }
    }

    //execute task group
    private void execute(TaskGroup group) {
        System.out.println("executing group:" + group);
        if (group.strategy == TaskStrategy.SYNCHRONOUS)
            synchronousExecuteTaskGroup(group);
        else
            asynchronousExecuteTaskGroup(group);
    }

    //synchronously execute task group tasks in main thread.
    private void synchronousExecuteTaskGroup(TaskGroup group) {
        if (group.getTaskList() != null && group.getTaskList().size() > 0) {
            //sort by priority
            List<TimerTask> sortList = new ArrayList<>(group.getTaskList().values());
            sortList.sort(new Comparator<TimerTask>() {
                @Override
                public int compare(TimerTask o1, TimerTask o2) {
                    return o2.getPriority() - o1.getPriority();
                }
            });
            sortList.forEach(this::doExecute0);
        }
    }

    private void asynchronousExecuteTaskGroup(TaskGroup group) {
        if ((group.getTaskList() == null || group.getTaskList().size() == 0) && (group.getSubGroups() == null || group.getSubGroups().size() == 0))
            return;
        setExecutorIfNull();

        //execute asynchronous tasks separately.
        if (group.getTaskList() != null && group.getTaskList().size() > 0) {
            for (TimerTask task : group.getTaskList().values()) {
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        doExecute0(task);
                    }
                });
            }
        }
        //execute asynchronous sub group.
        if (group.getSubGroups() != null && group.getSubGroups().size() > 0) {
            for (TaskGroup subGroup : group.getSubGroups()) {
                if (subGroup.getTaskList() != null && subGroup.getTaskList().size() > 0) {
                    executor.execute(new Runnable() {
                                         @Override
                                         public void run() {
                                             //sort by priority.DESC.
                                             List<TimerTask> sortList = new ArrayList<>(subGroup.getTaskList().values());
                                             sortList.sort(new Comparator<TimerTask>() {
                                                 @Override
                                                 public int compare(TimerTask o1, TimerTask o2) {
                                                     return o2.getPriority() - o1.getPriority();
                                                 }
                                             });
                                             for (TimerTask task : sortList)
                                                 doExecute0(task);
                                         }
                                     }
                    );
                }
            }
        }
    }

    //execute task.
    private void doExecute0(TimerTask task) {
        try {
            task.execute();
        } catch (TimerTaskExecutionException e) {
            e.printStackTrace();
        }
    }

    //apply task interval adjustment.
    private void applyTaskIntervalAdjustment() {
        if (originalTasks.isEmpty())
            return;
        originalTasks.forEach(TimerTask::adjustTaskIntervalIfNeeded);
    }

    //ticking when complete one more round schedule and execution.
    private void ticking() {
        count++;
        clock += cyclicInterval;

        System.out.println("clock:" + clock + ",task round:" + count + ",cyclicInterval:" + cyclicInterval);
    }

    //find appropriate cyclic interval for this main task manager thread run.
    private void applyMostAppropriateCyclicInterval() {
        if (originalTasks.isEmpty()) {
            this.cyclicInterval = 0;//no task for run..will not run at all.
            return;
        }
        //one task only.
        if (originalTasks.size() == 1) {
            this.cyclicInterval = originalTasks.get(0).getTimerSchedule().getScheduleInterval();
            return;
        }
        //we will get appropriate cyclic interval between each looping run .the gap minimum is 1 second.
        Set<Long> intervals = new HashSet<>();
        for (TimerTask task : originalTasks) {
            intervals.add(TimeUnit.MILLISECONDS.toSeconds(task.getTimerSchedule().getScheduleInterval()));
        }
        Long[] arrs = intervals.toArray(new Long[intervals.size()]);
        Arrays.<Long>sort(arrs);
        //use the first smallest N as the fence .find one number between 1 and N.which can be divided by all intervals.
        if (arrs[0] <= 1) {
            this.cyclicInterval = 1000;//milliseconds.
            return;
        }
        long fence = arrs[0];
        long finding = 1;
        outer:
        for (int i = (int) fence; i >= 0; i--) {
            for (int j = 1; j < arrs.length; j++) {
                //not matching.
                if (arrs[j] % i != 0)
                    break;

                //last match.
                if (j == arrs.length - 1) {
                    finding = i;
                    break outer;
                }
            }
        }
        this.cyclicInterval = finding * 1000;//to millis.
    }

    //build task groups
    private List<TaskGroup> buildInitialGroups() {
        if (originalTasks.isEmpty())
            return new ArrayList<>(0);
        List<TaskGroup> target = new ArrayList<>(originalTasks.size() >>> 2);
        originalTasks.forEach(task -> {
            if (task.isTaskReadyForRun(clock))
                applyTaskGroup(task, target);
        });
        return target;
    }

    //select some of the synchronous tasks into one group to run and others into asynchronous.
    //remove synchronous tasks from target .but return synchronous group.
    //the basic rules for adjustment is:
    //group all the tasks that could be together executed and the sum execution time should be less than cyclical interval
    private TaskGroup adjustGroup(final List<TaskGroup> target) {
        //make the index 0 for synchronous group for single time run in the main thread.
        TaskGroup synchronousGroup = null;
        TaskGroup anyGroup = null;
        TaskGroup result = null;
        for (TaskGroup group : target) {
            if (group.strategy == TaskStrategy.SYNCHRONOUS) {
                synchronousGroup = group;
                break;
            }
            //second best choice.
            if (group.strategy == TaskStrategy.ANY) {
                if (anyGroup == null)
                    anyGroup = group;
                else {
                    if (anyGroup.priority < group.priority)
                        anyGroup = group;//choose high priority task.
                }
            }
        }

        //choose second best group.
        if (synchronousGroup == null && anyGroup != null)
            synchronousGroup = anyGroup;

        if (synchronousGroup != null) {
            result = synchronousGroup.applySynchronousGroup();
            if (synchronousGroup.getTaskList().size() == 0)//all removed.
                target.remove(synchronousGroup);
        }

        //group asynchronous tasks in each group.
        target.forEach(TaskGroup::applyAsynchronousGroup);

        return result;
    }

    //determine the TaskGroup of task.
    private void applyTaskGroup(TimerTask task, final List<TaskGroup> groups) {
        TaskStrategy preferredStrategy = applyStrategy(task);
        if (groups.isEmpty()) {
            TaskGroup group = new TaskGroup(preferredStrategy, task.getPriority());
            group.addTask(task);
            groups.add(group);
            return;
        }

        TaskGroup preferredGroup = null;
        TaskGroup secondaryGroup = null;
        for (TaskGroup group : groups) {
            //first step of grouping by priority and strategy.

            //perfect match
            if (group.strategy == preferredStrategy && group.priority == task.getPriority()) {
                preferredGroup = group;
                break;
            }

            //find suitable secondary preferred group if possible.
            if (group.strategy.ordinal() >= preferredStrategy.ordinal() || (preferredStrategy.ordinal() >>> 1 == group.strategy.ordinal() && task.getPriority() <= group.priority)) {
                if (secondaryGroup == null) {
                    secondaryGroup = group;
                } else if (secondaryGroup.strategy.ordinal() < group.strategy.ordinal() && secondaryGroup.priority <= group.priority)
                    secondaryGroup = group;
            }
        }

        //second best suitable if not finding best match.
        if (preferredGroup == null && secondaryGroup != null) {
            preferredGroup = secondaryGroup;
        }
        //found perfect match.
        if (preferredGroup != null) {
            preferredGroup.addTask(task);
            return;
        }

        //not found. make a new group.
        preferredGroup = new TaskGroup(preferredStrategy, task.getPriority());
        preferredGroup.addTask(task);
        preferredGroup.setPriority(Math.max(task.getPriority(), preferredGroup.getPriority()));
        groups.add(preferredGroup);
    }

    //apply the appropriate task strategy.
    private TaskStrategy applyStrategy(TimerTask task) {
        //new task check interval.
        if (task.getStatistic().getTimes() == 0 || task.getPreferredStrategy() == TaskStrategy.ASYNCHRONOUS) {
            return TaskStrategy.ASYNCHRONOUS;//we can not risk if use synchronous strategy.
        } else {
            //runtime execution time and interval check.
            //exclude the most riskier tasks that could last longer than cyclic interval .
            if (task.getStatistic().getLongestExecutionTime() > cyclicInterval || task.getStatistic().getShortestExecutionTime() > cyclicInterval >>> 1)
                return TaskStrategy.ASYNCHRONOUS;
            //it is riskier to change the strategy to synchronous.the condition has to be precise.
            if (task.getStatistic().getTimes() > 100 && task.getStatistic().getLongestExecutionTime() < cyclicInterval && task.getStatistic().getAvgExecutionTime() < cyclicInterval >>> 2)
                return TaskStrategy.SYNCHRONOUS;
            if (task.getStatistic().getTimes() > 10 && task.getStatistic().getLongestExecutionTime() < cyclicInterval >>> 1 && task.getStatistic().getLastExecutionTime() < cyclicInterval >>> 2 && task.getStatistic().getAvgExecutionTime() < cyclicInterval >>> 1)
                return TaskStrategy.SYNCHRONOUS;
            if (task.getStatistic().getAvgExecutionTime() < cyclicInterval >>> 2 && task.getStatistic().getLongestExecutionTime() < cyclicInterval >>> 1)
                return TaskStrategy.ANY;//it is a little riskier with any.

            return task.getPreferredStrategy();
        }
    }

    //assign default executor if null for asynchronous exeuction..
    private void setExecutorIfNull() {
        if (executor == null) {
            executor = new ThreadPoolExecutor(5, 20, 60, TimeUnit.SECONDS, new SynchronousQueue<Runnable>(), new ThreadFactory() {
                ThreadGroup group = new ThreadGroup("TimerTaskGroup");
                AtomicInteger threadCounting = new AtomicInteger(0);

                @Override
                public Thread newThread(Runnable r) {
                    return new Thread(group, r, group.getName() + threadCounting.incrementAndGet());
                }
            }, new ThreadPoolExecutor.CallerRunsPolicy());

            Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                    executor.shutdown();
                }
            });
        }
    }

    //recurrent timer task group.it is used to group all tasks list with features: priority ,strategy
    class TaskGroup {
        //task execute strategy.this is preferred execution strategy. Executor may still not use the same strategy at runtime.
        TaskStrategy strategy;
        //priority
        int priority = 5;//default priority .aligned with thread.priority.
        //task list.the key is JobId. key helps finding task by job id.
        //task list in this map will be separately executed.
        Map<Long, TimerTask> taskList;
        //sub group tasks will be executed as a whole batch.
        List<TaskGroup> subGroups;

        //empty group with same strategy ,same priority.
        public TaskGroup(TaskStrategy strategy, int priority) {
            this.strategy = strategy;
            this.priority = priority;
        }

        //add task list into group with same priority.same strategy.
        public TaskGroup(TaskStrategy strategy, Map<Long, TimerTask> taskList) {
            this.strategy = strategy;
            this.taskList = new HashMap<>(taskList);
            if (this.taskList.size() > 0)
                this.priority = taskList.values().iterator().next().getPriority();
        }

        public void addTask(TimerTask timerTask) {
            if (taskList == null)
                taskList = new HashMap<>(4);
            taskList.put(timerTask.getJobId(), timerTask);
        }

        public TimerTask removeTask(long jobId) {
            return taskList.remove(jobId);
        }

        public void addSubGroup(TaskGroup subGroup) {
            if (this.subGroups == null)
                this.subGroups = new ArrayList<>(4);
            this.subGroups.add(subGroup);
        }

        //getter and setter

        public TaskStrategy getStrategy() {
            return strategy;
        }

        public void setStrategy(TaskStrategy strategy) {
            this.strategy = strategy;
        }

        public int getPriority() {
            return priority;
        }

        public void setPriority(int priority) {
            this.priority = priority;
        }

        public Map<Long, TimerTask> getTaskList() {
            return taskList;
        }

        public void setTaskList(Map<Long, TimerTask> taskList) {
            this.taskList = taskList;
        }

        public List<TaskGroup> getSubGroups() {
            return subGroups;
        }

        public void setSubGroups(List<TaskGroup> subGroups) {
            this.subGroups = subGroups;
        }

        //pick up few SYNCHRONOUS tasks in one group which sums all execution time less than cyclic interval.
        //return synchronous tasks in one group
        private TaskGroup applySynchronousGroup() {
            //asynchronous task group never can be executed synchronously.
            if (getTaskList() == null || getTaskList().isEmpty() || getStrategy() == TaskStrategy.ASYNCHRONOUS)
                return null;
            long fence = cyclicInterval;
            //sorting by execution time in ascending order.
            List<TimerTask> temp = new ArrayList<>(getTaskList().values());
            temp.sort(new Comparator<TimerTask>() {
                @Override
                public int compare(TimerTask o1, TimerTask o2) {
                    return (int) (o1.getStatistic().getAvgExecutionTime() - o2.getStatistic().getAvgExecutionTime());
                }
            });

            //picking up tasks in order.
            Map<Long, TimerTask> syncGroup = new HashMap<>(4);
            long sumTime = 0;
            for (TimerTask task : temp) {
                if ((sumTime = sumTime + task.getStatistic().getAvgExecutionTime()) > fence)
                    break;
                syncGroup.put(task.getJobId(), task);
            }

            if (syncGroup.size() > 0) {
                //transfer the group.
                TaskGroup result = new TaskGroup(TaskStrategy.SYNCHRONOUS, syncGroup);
                for (Long jobId : syncGroup.keySet())
                    removeTask(jobId);
                return result;
            }
            return null;
        }

        //pick up ASYNCHRONOUS tasks in one group which sum all execution time  less than cyclic interval.
        //split tasks into sub groups for synchronous execution.
        private void applyAsynchronousGroup() {
            if (getTaskList() == null || getTaskList().isEmpty())
                return;
            long fence = TimerTaskManager.this.cyclicInterval;
            //sorting by execution time in ascending order.
            List<TimerTask> temp = new ArrayList<>(getTaskList().values());
            temp.sort(new Comparator<TimerTask>() {
                @Override
                public int compare(TimerTask o1, TimerTask o2) {
                    return (int) (o1.getStatistic().getAvgExecutionTime() - o2.getStatistic().getAvgExecutionTime());
                }
            });

            //picking up tasks in order..each sub task in one sub group should be executed together synchronously.
            TaskGroup subGroup = null;
            long sumTime = 0;
            for (TimerTask task : temp) {
                if (task.getStatistic().getTimes() <= 100
                        || task.getPreferredStrategy() == TaskStrategy.ASYNCHRONOUS
                        || task.getStatistic().getLongestExecutionTime() > cyclicInterval
                        || task.getStatistic().getAvgExecutionTime() > cyclicInterval >>> 1)//we have to make sure the tasks added into sub group are no risk for execution together.
                    continue;
                if ((sumTime + task.getStatistic().getAvgExecutionTime()) > fence) {
                    //there is indeed sub tasks.
                    if (subGroup != null && subGroup.getTaskList() != null && subGroup.getTaskList().size() > 0) {
                        addSubGroup(subGroup);
                        //prepare for next sub group.
                        subGroup = new TaskGroup(TaskStrategy.SYNCHRONOUS, 5);
                    }
                    sumTime = 0;//reset sum time.
                }
                if (subGroup == null)
                    subGroup = new TaskGroup(TaskStrategy.SYNCHRONOUS, task.getPriority());
                sumTime += task.getStatistic().getAvgExecutionTime();
                subGroup.addTask(task);
                subGroup.setPriority(Math.max(task.getPriority(), subGroup.getPriority()));
                //remove from sub tasks list.
                removeTask(task.getJobId());
            }
        }
        @Override
        public String toString() {
            return "TaskGroup{" +
                    "strategy=" + strategy +
                    ", priority=" + priority +
                    ", taskList=" + taskList +
                    ", subGroups=" + subGroups +
                    '}';
        }
    }
}
